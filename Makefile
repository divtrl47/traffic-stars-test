bid:
	go build -o=build/bidservice cmd/bidservice/main.go
	BID_SERVICE_PORT=3000 nohup ./build/bidservice &

source:
	nohup go run cmd/fasttestsource/main.go &

test-bid:
	cd cmd/bidservice && go test bid_test.go

test-hash:
	cd hashmap/ && go test -v

all: source bid test-bid test-hash

.PHONY: all bid source bid-test
