## Tests
```
make
```

### Hash Map

operations: 100000

| size | collisions | allocations |
|------|------------|-------------|
|   16 |      46940 |           8 |
|   64 |      43892 |           7 |
|  128 |      46457 |           7 |
| 1024 |      44591 |           5 |

```
make test-hash
```

### Bid Service

```
make bid
make source
make test-bid
```

env:

BID_SERVICE_PORT = 3000