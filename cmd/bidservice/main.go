package main

import (
	"net/http"
	"os"
	"io/ioutil"
	"encoding/json"
	"sort"
	"time"
	"errors"
	"fmt"
)

const DefaultPort = "3000"

const Timeout = 100 * time.Millisecond
const AverageTime = 14 * time.Millisecond

const SourceParam = "s"

const (
	HandlerWinner = "/winner"
)

type Price struct {
	Price int `json:"price"`
}

type source struct {
	prices []int
	url    string
}

var ErrNotEnoughPrices = errors.New("not_enough_prices")

var httpClient = &http.Client{
	Timeout: Timeout,
}

type winnerResponse struct {
	Price  int    `json:"price"`
	Source string `json:"source"`
	Error  string `json:"error"`
}

func main() {
	port := os.Getenv("BID_SERVICE_PORT")
	if port == "" {
		port = DefaultPort
	}

	http.HandleFunc(HandlerWinner, winnerHandler)

	err := http.ListenAndServe(":"+port, nil)
	if err != nil {
		panic(err)
	}
}

func winnerHandler(w http.ResponseWriter, r *http.Request) {

	queryValues := r.URL.Query()
	var prices []int
	var sources []source

	for k, v := range queryValues {
		if k != SourceParam {
			continue
		}

		for _, url := range v {
			go func(url string) {
				src, err := getSource(url)
				if err != nil {
					return
				}
				sources = append(sources, src)
				prices = append(prices, src.prices...)
			}(url)
		}
	}

	time.Sleep(Timeout - AverageTime)

	sort.Slice(sources, func(i, j int) bool {
		return sources[i].prices[0] > sources[j].prices[0]
	})

	sort.Slice(prices, func(i, j int) bool {
		return prices[i] > prices[j]
	})

	if len(prices) < 2 {
		writeResponse(w, winnerResponse{
			Error: ErrNotEnoughPrices.Error(),
		})
		return
	}

	fmt.Println(prices, sources)
	writeResponse(w, winnerResponse{
		Source: sources[0].url,
		Price:  prices[1],
	})
}

func getSource(url string) (src source, err error) {
	src = source{url: url}
	resp, err := httpClient.Get(url)
	if err != nil {
		return
	}

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}

	var prices []Price
	err = json.Unmarshal(data, &prices)
	if err != nil {
		return
	}

	sort.Slice(prices, func(i, j int) bool {
		return prices[i].Price > prices[j].Price
	})

	for i := 0; i < len(prices) && i < 2; i++ {
		src.prices = append(src.prices, prices[i].Price)
	}
	if len(src.prices) <= 0 {
		src.prices = append(src.prices, 0)
	}

	return
}

func makeTimestamp() time.Duration {
	return time.Duration(time.Now().UnixNano() / int64(time.Millisecond))
}

func writeResponse(w http.ResponseWriter, data interface{}) {
	resp, err := json.Marshal(data)
	if err != nil {
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(resp)

	return
}
