package main

import (
	"net/http"
	"os"
	"testing"
	"fmt"
	"io/ioutil"
	"encoding/json"
)

func TestFastBid(t *testing.T) {
	client := http.Client{
		Timeout: Timeout,
	}

	port := os.Getenv("BID_SERVICE_PORT")
	if port == "" {
		port = DefaultPort
	}

	var testData = map[string]winnerResponse{
		"s=http://127.0.0.1:8080/primes": winnerResponse{
			Price:  19,
			Source: "http://127.0.0.1:8080/primes",
		},
		"s=http://127.0.0.1:8080/primes&s=http://127.0.0.1:8080/fibo&s=http://127.0.0.1:8080/fact": winnerResponse{
			Price:  23,
			Source: "http://127.0.0.1:8080/fact",
		},
		"s=http://127.0.0.1:8080/primes&s=http://127.0.0.1:8080/fibo&s=http://127.0.0.1:8080/fact&s=http://127.0.0.1:8080/rand": winnerResponse{
			Price:  34,
			Source: "http://127.0.0.1:8080/rand",
		},
		"s=http://127.0.0.1:8080/empty": winnerResponse{
			Price:  0,
			Source: "",
			Error:  ErrNotEnoughPrices.Error(),
		},
	}

	for params, data := range testData {
		response, err := client.Get(fmt.Sprintf("http://127.0.0.1:%s/winner?%s", port, params))
		if err != nil {
			t.Error(err)
			continue
		}

		b, err := ioutil.ReadAll(response.Body)
		if err != nil {
			t.Error(err)
			continue
		}

		winnerRes := winnerResponse{}

		err = json.Unmarshal(b, &winnerRes)
		if err != nil {
			t.Error(err)
			continue
		}

		if winnerRes.Source != data.Source || winnerRes.Price != data.Price || winnerRes.Error != data.Error {
			t.Error("Wrong response", winnerRes.Price, data.Price, params)
		}
	}

}
