package hashmap

import (
	"testing"
	"math/rand"
	"time"
)

type TestHashMaper = map[interface{}]interface{}

func TestNewHashMap(t *testing.T) {
	const operationsCnt = 1000000
	testSizes := []int{16, 64, 128, 1024}

	t.Logf("operations: %d", operationsCnt)
	t.Logf("size | collisions | allocations")
	for _, size := range testSizes {
		testData := makeRandomMap(operationsCnt)

		hashMapper := NewHashMap(size, nil)
		for key, value := range testData {
			if err := hashMapper.Set(key, value); err == ErrCollision {
				t.Fatalf("set error: key: %s, value: %s", key, value)
			}
		}

		for key, value := range testData {
			hval, err := hashMapper.Get(key)
			if err == ErrInvalidKey || value != hval {
				t.Fatalf("get error: key: %s, value: %s|%s", key, value, hval)
			}
		}

		for key, value := range testData {
			err := hashMapper.Unset(key)
			if err == ErrInvalidKey {
				t.Fatalf("unset error: key: %s, value: %s", key, value)
			}
		}

		stat := hashMapper.Statistic()
		t.Logf("%4d | %10d | %11d", size, stat.collisionCnt, stat.allocationCnt)
	}

}

// test utils
func makeRandomMap(size int) (testData TestHashMaper) {
	testData = make(TestHashMaper, size)
	var rnd = rand.New(rand.NewSource(time.Now().UnixNano()))

	for i := 0; i < size; i++ {
		testData[randStringRunes(rnd, 8)] = randStringRunes(rnd, 8)
	}

	return
}

var runes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func randStringRunes(r *rand.Rand, n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = runes[r.Intn(len(runes))]
	}
	return string(b)
}
